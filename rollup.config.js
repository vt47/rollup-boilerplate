import babel from 'rollup-plugin-babel'
import uglify from 'rollup-plugin-uglify'
import serve from 'rollup-plugin-serve'
import livereload from 'rollup-plugin-livereload'
import scss from 'rollup-plugin-scss'
import eslint from 'rollup-plugin-eslint'
import replace from 'rollup-plugin-replace'
import url from "rollup-plugin-url"
import commonjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'
import jsx from 'rollup-plugin-jsx'
// import inject from 'rollup-plugin-inject'
// import collectSass from 'rollup-plugin-collect-sass'
import cssnano from 'cssnano'
import cssnext from 'postcss-cssnext'
import postcss from 'postcss'

export default {
  input: 'src/index.js',
  output: {
    file: 'dist/index.js',
    format: 'iife',
    sourcemap: true
  },
  plugins: [
    // Piping process
    // Src processed in module order, modify with care.
    // Linting && Transpiling original code
    eslint({
      exclude: [
        'static/**',
        'src/style/**',
        'src/assets/**'
      ]
    }),
    /* Uncomment jsx plugin if needed
    jsx({
      exclude: ['src/style/**', 'src/assets/**', 'node_modules/**'],
      factory: 'dio.createElement'
    }),
    */
    babel({
      exclude: [
        'node_modules/**',
        'src/style/**',
        'src/assets/**'
      ]
    }),
    scss({
      processor: css => postcss([cssnano, cssnext])
        .process(css).then(result => result.css),
      output: 'dist/style.css'
    }),
    url({
      limit: 50 * 1024
    }),

    // Resolve and include node modules
    resolve({
      jsnext: true,
      main: true,
      browser: true
    }),
    commonjs(),

    // Setup ENV and minify resource
    replace({
      exclude: 'node_modules/**',
      ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
    }),
    (process.env.NODE_ENV === 'production' && uglify()),

    // Init and runserver
    serve({
      contentBase: ['dist', 'static'],
      host: 'localhost',
      port: 3000,
      verbose: true
    }),
    livereload({
      watch: 'dist',
      listen: 3000
    })
  ],
}
